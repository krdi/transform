#!/usr/bin/env python2

import argparse
import sys
import subprocess
import time
import datetime
import string


class translatorTool(object):

    def __init__(self, args, lf):
        self.kb1 = ['1','2','3','4','5','6','7','8','9','0']
        self.kb2 = ['q','w','e','r','t','y','u','i','o','p']
        self.kb3 = ['a','s','d','f','g','h','j','k','l',';']
        self.kb4 = ['z','x','c','v','b','n','m',',','.','/']
        self.kbt1 = self.kb1
        self.kbt2 = self.kb2
        self.kbt3 = self.kb3
        self.kbt4 = self.kb4
        self.transformIn = [self.kb1, self.kb2, self.kb3, self.kb4]
        self.transformOut = [self.kbt1, self.kbt2, self.kbt3, self.kbt4]
        self.kbColumns = len(self.kb1)
        self.kbRows = len(self.transformIn)

        self.transformInS = ''
        for i in range(len(self.transformIn)):
            self.transformInS += ''.join(self.transformIn[i])

        self.completed = 0

        self.translationTable = ''
	self.transform =''

    def completed(self):
        return(self.completed)

    def doHorizontal(self):
	self.transform += 'H'
        transformTmp = [[0] * 10, [0] * 10, [0] * 10, [0] * 10]
        for i in range(self.kbColumns):
            for j in range(self.kbRows):
                transformTmp[j][i] = self.transformOut[j][self.kbColumns-1-i]
        for i in range(self.kbColumns):
            for j in range(self.kbRows):
                self.transformOut[j][i] = transformTmp[j][i]

    def doVertical(self):
	self.transform += 'V'
        transformTmp = [[0] * 10, [0] * 10, [0] * 10, [0] * 10]
        for i in range(self.kbColumns):
            for j in range(self.kbRows):
                transformTmp[j][i] = self.transformOut[self.kbRows-j-1][i]
        for i in range(self.kbColumns):
            for j in range(self.kbRows):
                self.transformOut[j][i] = transformTmp[j][i]

    def doShift(self, shift):
	self.transform += str((-1)*shift)
        transformTmp = [[0] * 10, [0] * 10, [0] * 10, [0] * 10]
        for i in range(self.kbColumns):
            for j in range(self.kbRows):
                current = j * self.kbColumns + i + shift + self.kbColumns * self.kbRows
                r = int(current % (self.kbColumns * self.kbRows) / self.kbColumns)
                c = (self.kbColumns * self.kbRows + current) % self.kbColumns
                transformTmp[j][i] = self.transformOut[r][c]
        for i in range(self.kbColumns):
            for j in range(self.kbRows):
                self.transformOut[j][i] = transformTmp[j][i]

    def doTranslate(self):
        self.transformOutS = ''
        for i in range(len(self.transformOut)):
            self.transformOutS += ''.join(self.transformOut[i])
        self.translationTable = string.maketrans(self.transformInS, self.transformOutS)

    def resetTransform():
	self.transform =''

    def Text(self, text):
        transformedText = text.translate(self.translationTable)
        self.completed += 1
        return(transformedText)

    def message(self, msg):
        self.logfile.write("**** " + msg + "\n")


def setup_argparser():
    parser = argparse.ArgumentParser(description='inputs a transformation and text, outputs translation. Sample use to run program:\necho -e "HV-1\nTEXT Hello\nTEXT go home" | python tr.py')
    parser.add_argument('-infile', help='if file input', required=False)
    parser.add_argument('-log', help='use log file', required=False)
    return parser.parse_args()


def getTransformData(args):
    if args.infile is None:
        return sys.stdin.readlines()
    else:
        return open(pwd + '/' + args.infile, 'rb').readlines()


def job():
    args = setup_argparser()
    lfTime = datetime.datetime.fromtimestamp(time.time()).strftime('%Y%m%d_%H%M%S')
    if args.log is None:
        lf = sys.stdout
    else:
        lfTime = datetime.datetime.fromtimestamp(time.time()).strftime('%Y%m%d_%H%M%S')
        lf = open(args.log + "_" + lfTime + ".log", "a", 1)
    lf.write("Starting translator\n")

    for transStuff in getTransformData(args):
        if transStuff[0:4] == "TEXT":
            translate.doTranslate()
            lf.write("Input line: %s; Transform: %s; Transformed line: %s" % (transStuff[5:].rstrip("\n\r"), translate.transform, translate.Text(transStuff[5:])))
        else:
            translate = translatorTool(args, lf)
            sign = ''
            for iopr in list(transStuff.rstrip("\n\r")):
                if iopr == 'H':
                    translate.doHorizontal()
                elif iopr == 'V':
                    translate.doVertical()
                elif iopr == '+' or iopr == '-':
                    sign = iopr
                elif iopr.isdigit() :
                    if sign == '-':
                        translate.doShift(int(iopr))
                    else:
                         translate.doShift((-1) * int(iopr))
                else:
                    lf.write("Unknown Operand %s in %s " % (iopr, transStuff))


if __name__ == "__main__":
    pwd = subprocess.check_output(['pwd']).replace("\n", "")
    job()

